<?php
/**
 * Template for footer
 *
 * 
 */
?>     
<footer class="bottom">
  <div class="bottominner widgets-style1 widgets-style2">
    <div class="container">
      <div class="row">
        <div class="col-lg-10">   
          <?php if ( is_active_sidebar( 'sidebar-2' ) ) : ?>
          <?php dynamic_sidebar( 'sidebar-2' ); ?>	
          <?php endif; ?>
        </div>
        <div class="col-lg-2">   
          <div class="scrollbutton">
             <a href="javascript:scrollToTop()" title="go to top"><i class="fa fa-chevron-up"></i></a>
          </div> 
        </div>
      </div>
	 </div>
   </div>
   <div class="bottominner1 widgets-style1">
    <div class="container">
	  <div class="row">
        <div class="col-sm-4">   
          <?php if ( is_active_sidebar( 'sidebar-3' ) ) : ?>
          <?php dynamic_sidebar( 'sidebar-3' ); ?>	
          <?php endif; ?>
        </div>
		  <div class="col-sm-4">   
          <?php if ( is_active_sidebar( 'sidebar-4' ) ) : ?>
          <?php dynamic_sidebar( 'sidebar-4' ); ?>	
          <?php endif; ?>
        </div>
		  <div class="col-sm-4">   
          <?php if ( is_active_sidebar( 'sidebar-5' ) ) : ?>
          <?php dynamic_sidebar( 'sidebar-5' ); ?>	
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>	
  <div class="wrapper100percent copyrightwrapper">
	  <div class="container">
		  <div class="col-lg-12 copyright">
			  <div class="footer-bottom__left">
				  <p><?php echo get_theme_mod('copyright_details'); ?></p>
			  </div>
			  <div class="footer-bottom__right">
				  <a href="http://naranja-studio.com" style="opacity: .65" onmouseover="this.style.opacity=1" onmouseout="this.style.opacity=.7">
					  <img src="//naranja-studio.com/designedby.svg" style="height:32px; width:32px">
				  </a>
			  </div>
		  </div>
	  </div>
  </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>