<?php
/**
 * This file represents an example of the code that themes would use to register
 * the required plugins.
 *
 * It is expected that theme authors would copy and paste this code into their
 * functions.php file, and amend to suit.
 *
 * @package	   TGM-Plugin-Activation
 * @subpackage Example
 * @version	   2.3.6
 * @author	   Thomas Griffin <thomas@thomasgriffinmedia.com>
 * @author	   Gary Jones <gamajo@gamajo.com>
 * @copyright  Copyright (c) 2012, Thomas Griffin
 * @license	   http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       https://github.com/thomasgriffin/TGM-Plugin-Activation
 */
/**
 * Include the TGM_Plugin_Activation class.
 */
require_once dirname( __FILE__ ) . '/class-tgm-plugin-activation.php';
add_action( 'tgmpa_register', 'rt_activism_register_required_plugins' );
/**
 * Register the required plugins for this theme.
 *
 * In this example, we register two plugins - one included with the TGMPA library
 * and one from the .org repo.
 *
 * The variable passed to tgmpa_register_plugins() should be an array of plugin
 * arrays.
 *
 * This function is hooked into tgmpa_init, which is fired within the
 * TGM_Plugin_Activation class constructor.
 */
function rt_activism_register_required_plugins() {
/**
	 * Array of plugin arrays. Required keys are name and slug.
	 * If the source is NOT from the .org repo, then source is also required.
	 */
	$plugins = array(

		// This is an example of how to include a plugin pre-packaged with a theme
		array(
			'name'     				=> 'Rt activismtheme functions', // The plugin name
			'slug'     				=> 'rt-activismtheme-functions', // The plugin slug (typically the folder name)
			'source'   				=> get_stylesheet_directory() . '/plugins/rt-activismtheme-functions.zip', // The plugin source
			'required' 				=> true, // If false, the plugin is only 'recommended' instead of required
			'version' 				=> '', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
			'force_activation' 		=> false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
			'force_deactivation' 	=> false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
			'external_url' 			=> '', // If set, overrides default API URL and points to an external URL
		),
		
            	array(
			'name'     				=> 'Slider Revolution', // The plugin name
			'slug'     				=> 'revslider', // The plugin slug (typically the folder name)
			'source'   				=> get_stylesheet_directory() . '/plugins/revslider.zip', // The plugin source
			'required' 				=> true, // If false, the plugin is only 'recommended' instead of required
			'version' 				=> '', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
			'force_activation' 		=> false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
			'force_deactivation' 	=> false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
			'external_url' 			=> '', // If set, overrides default API URL and points to an external URL
		),
                

		// This is an example of how to include a plugin from the WordPress Plugin Repository
		              array(
			'name' 		=> 'aqua-page-builder',
			'slug' 		=> 'aqua-page-builder',
			'required' 	=> true,
		),
		  array(
			'name' 		=> 'the-events-calendar',
			'slug' 		=> 'the-events-calendar',
			'required' 	=> false,
		),
                
                   array(
			'name' 		=> 'contact-form-7',
			'slug' 		=> 'contact-form-7',
			'required' 	=> true,
		),
                  array(
			'name' 		=> 'wordpress-easy-paypal-payment-or-donation-accept-plugin',
			'slug' 		=> 'wordpress-easy-paypal-payment-or-donation-accept-plugin',
			'required' 	=> false,
		),

           array(
			'name' 		=> 'wp-less',
			'slug' 		=> 'wp-less',
			'required' 	=> false,
		),
             array(
			'name' 		=> 'post-types-order',
			'slug' 		=> 'post-types-order',
			'required' 	=> false,
		),


           array(
			'name' 		=> 'font-awesome-more-icons',
			'slug' 		=> 'font-awesome-more-icons',
			'required' 	=> false,
		),
                 
  array(
			'name' 		=> 'polylang',
			'slug' 		=> 'polylang',
			'required' 	=> false,
		),
               

	);

	// Change this to your theme text domain, used for internationalising strings
	$activism = 'tgmpa';

	/**
	 * Array of configuration settings. Amend each line as needed.
	 * If you want the default strings to be available under your own theme domain,
	 * leave the strings uncommented.
	 * Some of the strings are added into a sprintf, so see the comments at the
	 * end of each line for what each argument will be.
	 */
	$config = array(
		'domain'       		=> $activism,         	// Text domain - likely want to be the same as your theme.
		'default_path' 		=> '',                         	// Default absolute path to pre-packaged plugins
		'parent_menu_slug' 	=> 'themes.php', 				// Default parent menu slug
		'parent_url_slug' 	=> 'themes.php', 				// Default parent URL slug
		'menu'         		=> 'install-required-plugins', 	// Menu slug
		'has_notices'      	=> true,                       	// Show admin notices or not
		'is_automatic'    	=> false,					   	// Automatically activate plugins after installation or not
		'message' 			=> '',							// Message to output right before the plugins table
		'strings'      		=> array(
			'page_title'                       			=> __( 'Install Required Plugins', 'activism' ),
			'menu_title'                       			=> __( 'Install Plugins', 'activism' ),
			'installing'                       			=> __( 'Installing Plugin: %s', 'activism' ), // %1$s = plugin name
			'oops'                             			=> __( 'Something went wrong with the plugin API.', 'activism' ),
			'notice_can_install_required'     			=> _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.', 'activism' ), // %1$s = plugin name(s)
			'notice_can_install_recommended'			=> _n_noop( 'This theme recommends the following plugin: %1$s.',  'This theme recommends the following plugins: %1$s.', 'activism' ), // %1$s = plugin name(s)
			'notice_cannot_install'  					=> _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.', 'activism' ), // %1$s = plugin name(s)
			'notice_can_activate_required'    			=> _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.', 'activism' ), // %1$s = plugin name(s)
			'notice_can_activate_recommended'			=> _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.', 'activism' ), // %1$s = plugin name(s)
			'notice_cannot_activate' 					=> _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.', 'activism' ), // %1$s = plugin name(s)
			'notice_ask_to_update' 						=> _n_noop( 'The following plugin needs to be updated to activism latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'activism' ), // %1$s = plugin name(s)
			'notice_cannot_update' 						=> _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.', 'activism' ), // %1$s = plugin name(s)
			'install_link' 					  			=> _n_noop( 'Begin installing plugin', 'Begin installing plugins', 'activism' ),
			'activate_link' 				  			=> _n_noop( 'Activate installed plugin', 'Activate installed plugins', 'activism' ),
			'return'                           			=> __( 'Return to Required Plugins Installer', 'activism' ),
			'plugin_activated'                 			=> __( 'Plugin activated successfully.', 'activism' ),
			'complete' 									=> __( 'All plugins installed and activated successfully. %s', 'activism' ), // %1$s = dashboard link
			'nag_type'									=> 'updated' // Determines admin notice type - can only be 'updated' or 'error'
		)
	);

	tgmpa( $plugins, $config );

}


add_action('init', 'process_post');

function process_post(){
 if(isset($_POST['unique_hidden_field'])) {
   // process $_POST data here
 }
}

/**********************************************************
* include metabox 
***********************************************************/

// Re-define meta box path and URL
define( 'RWMB_URL', trailingslashit( get_stylesheet_directory_uri() . '/meta-box' ) );
define( 'RWMB_DIR', trailingslashit( TEMPLATEPATH . '/meta-box' ) );
// Include the meta box script
require_once RWMB_DIR . 'meta-box.php';
// Include the meta box definition
include 'assets/php/config-meta-boxes.php';
include 'assets/php/meta-boxes.php';
include 'assets/php/custom-styles.php';


/**********************************************************
* language support
***********************************************************/

load_theme_textdomain( 'activism', TEMPLATEPATH.'/languages' );
 
$locale = get_locale();
$locale_file = TEMPLATEPATH."/languages/$locale.php";
if ( is_readable($locale_file) )
	require_once($locale_file);


/**********************************************************
* google fonts support
***********************************************************/

function rt_activism_fonts() {
$protocol = is_ssl() ? 'https' : 'http';
wp_enqueue_style( 'rt_activism-opensans', "$protocol://fonts.googleapis.com/css?family=Open+Sans:400,700" );
}

add_action( 'wp_enqueue_scripts', 'rt_activism_fonts' );




/**********************************************************
* title function
***********************************************************/

function rt_activism_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() )
		return $title;

	// Add the site name.
	$title .= get_bloginfo( 'name' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		$title = "$title $sep $site_description";

	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 )
		$title = "$title $sep " . sprintf( __( 'Page %s', 'activism' ), max( $paged, $page ) );

	return $title;
}
add_filter( 'wp_title', 'rt_activism_wp_title', 10, 2 );


/**********************************************************
* menu
***********************************************************/

register_nav_menu( 'primary-menu', __( 'Primary Menu', 'activism' ) );
register_nav_menu( 'header-menu', __( 'Header Menu', 'activism' ) );


/**********************************************************
* support for shortcodes in excerpt 
***********************************************************/
add_filter('the_excerpt', 'do_shortcode');


/**********************************************************
* post width
***********************************************************/
if ( ! isset( $content_width ) ) $content_width = 900;


/******************************************************
* enable image support
*******************************************************/

load_theme_textdomain( 'activism', get_template_directory() . '/languages' );

add_theme_support( 'automatic-feed-links' );


if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'post-thumbnails', array( 'post', 'page','postsecond', 'homeblog', 'team' ) );      
}

if ( function_exists( 'add_image_size' ) ) { 

//one third - onethirdimage class for homepage blog
add_image_size( 'onethird-large', 351, 351, true ); 
add_image_size( 'onethird-medium', 440, 440, true); 
add_image_size( 'onethird-small', 210, 210, true ); 
add_image_size( 'onethird-thumb', 280, 280, true ); 

//one forth - oneforthimage class for team
add_image_size( 'oneforth-large', 254, 9999 ); 
add_image_size( 'oneforth-medium', 210, 9999); 
add_image_size( 'oneforth-small', 330, 9999 ); 
add_image_size( 'oneforth-thumb', 250, 9999 ); 

}

/************************************************************************
* Scripts and styles 
*************************************************************************/

/***function for jqery scripts***/


function activism_scripts() {


wp_enqueue_style( 'style', get_stylesheet_uri() );

wp_enqueue_style( 'styleless', get_stylesheet_directory_uri(). '/style.less' );

wp_enqueue_script( 'jquery');


 if ( is_singular() ) wp_enqueue_script( "comment-reply" );


wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.js', array('jquery'), '1.0.0', true );
wp_enqueue_script( 'masonry', get_template_directory_uri() . '/assets/js/masonry.js', array('jquery'), '1.0.0', true, '1.0.0', true );
wp_enqueue_script( 'parallax', get_template_directory_uri() . '/assets/js/parallax.js', array('jquery'), '1.0.0', true );
wp_enqueue_script( 'sticky', get_template_directory_uri() . '/assets/js/sticky.js', array('jquery'), '1.0.0', true );
wp_enqueue_script( 'waypoints', get_template_directory_uri() . '/assets/js/waypoints.js', array('jquery'), '1.0.0', true );
wp_enqueue_script( 'colorbox', get_template_directory_uri() . '/assets/js/colorbox.js', array('jquery'), '1.0.0', true );
wp_enqueue_script( 'owl', get_template_directory_uri() . '/assets/js/owl-carousel.js', array('jquery'), '1.0.0', true );
wp_enqueue_script( 'bx', get_template_directory_uri() . '/assets/js/bxslider.js', array('jquery'), '1.0.0', true );
wp_enqueue_script( 'retina', get_template_directory_uri() . '/assets/js/retina.js', array('jquery'), '1.0.0', true );

wp_enqueue_script( 'scripts', get_template_directory_uri() . '/assets/js/scripts.js', array('jquery'), '1.0.0', true );
}


add_action( 'wp_enqueue_scripts', 'activism_scripts' );



/************************************************************************
* customize theme
*************************************************************************/

function rt_activism_customize_register( $wp_customize ) {


//customize copyright details
$wp_customize->add_setting( 'copyright_details' , array(
    'default'     => 'your copyright details',
    'transport'   => 'refresh',
    'sanitize_callback' => 'example_sanitize_text',
) );

$wp_customize->add_section( 'activism_copyright' , array(
    'title'      => __( 'Copyright Details', 'activism' ),
   
) );



$wp_customize->add_control( 'copyright_details', array(
	'label'        => __( 'Copyright', 'activism' ),
	'section'    => 'activism_copyright',
	'settings'   => 'copyright_details',
 ));


//customize logo
$wp_customize->add_setting( 'logo', array(
    'transport'   => 'refresh',
    'sanitize_callback' => 'esc_url_raw',
) );

$wp_customize->add_section( 'activism_logo_section' , array(
    'title'       => __( 'Logo', 'activism' ),
    'priority'    => 30,
    'description' => __('Upload a logo to replace the default site name and description in the header', 'activism' ),
) );

$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'logo', array(
    'label'    => __( 'Logo', 'activism' ),
    'section'  => 'activism_logo_section',
    'settings' => 'logo',
) ) );


}

add_action( 'customize_register', 'rt_activism_customize_register' );


function example_sanitize_text( $input ) {
    return wp_kses_post( force_balance_tags( $input ) );
}



/************************************************************************
* Admin scripts and styles
*************************************************************************/


function register_admin_scripts() {
wp_enqueue_script( 'metaupload', get_template_directory_uri() . '/admin/assets/js/meta-box-upload.js' );
}

add_action( 'admin_enqueue_scripts', 'register_admin_scripts' );




/****************************************************************
* Sidebars and widget areas
*****************************************************************/


function activism_widgets_init() {
	register_sidebar( array(
		'name' => __( 'Main Blog Sidebar', 'activism' ),
		'id' => 'sidebar-1',
		'description' => __( 'Appears in posts and pages', 'activism' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title"><span>',
		'after_title' => '</span></h3>',
	) );

	register_sidebar( array(
		'name' => __( 'Footer First', 'activism' ),
		'id' => 'sidebar-2',
		'description' => __( 'Appears in footer. For icons.', 'activism' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	register_sidebar( array(
		'name' => __( 'Footer Second', 'activism' ),
		'id' => 'sidebar-3',
		'description' => __( 'Appears in footer', 'activism' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	register_sidebar( array(
		'name' => __( 'Footer Third', 'activism' ),
		'id' => 'sidebar-4',
		'description' => __( 'Appears in footer', 'activism' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	register_sidebar( array(
		'name' => __( 'Footer Forth', 'activism' ),
		'id' => 'sidebar-5',
		'description' => __( 'Appears in footer', 'activism' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	register_sidebar( array(
		'name' => __( 'Header First', 'activism' ),
		'id' => 'sidebar-6',
		'description' => __( 'Appears in header', 'activism' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	register_sidebar( array(
		'name' => __( 'Header Second', 'activism' ),
		'id' => 'sidebar-7',
		'description' => __( 'Appears in header', 'activism' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );


	
}
add_action( 'widgets_init', 'activism_widgets_init' );

/************************************************************************
* page/post navigation
*************************************************************************/


add_filter( 'wp_nav_menu_objects', 'nav_links' );
function nav_links( $abcs ) {
foreach  ($abcs as $abc ) {
if('custom' == $abc->type && !is_page()){
if( 1 == preg_match('/^#([^\/]+)$/', $abc->url )){
$abc->url = home_url( '/' ).$abc->url;
}

}
	
}

return $abcs;   
}


/************************************************************************
* for aqua pagebuilder plugin
*************************************************************************/


require_once('page-builder/page-builder.php');



