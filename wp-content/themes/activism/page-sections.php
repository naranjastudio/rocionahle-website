<?php
/**********************************************************
* Page Section custom post type
***********************************************************/

$currentid = get_the_ID();?>
<?php
$checkbox2 = rwmb_meta('activism_checkbox2'); 
?>
<div class="wrapper100percent section1 <?php if($checkbox2) echo "section1color"; ?> <?php echo get_post_type();?>-<?php the_ID();?> " id="<?php echo $post->post_name;?>">
  
<div class="sectionwrapper"> </div> 
<?php
$checkbox = rwmb_meta('activism_checkbox'); 
?>
<div class="<?php if($checkbox) echo "container"; ?>"> 

<?php the_content(); ?>

</div> 
</div> 

<div class="wrapper100percent"> 
<?php $themeparallax = get_post_meta($currentid, 'its_parallax_attach', TRUE);
if( $themeparallax && $themeparallax !='none' ){
global $rt_itstheme_post;
$rt_itstheme_post = $post;
$parallax_post = get_post($themeparallax); 
setup_postdata( $parallax_post );
$post = $parallax_post;
get_template_part('/parallax-section');
wp_reset_postdata();
}?>
</div> 

