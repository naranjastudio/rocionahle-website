<?php

function nahle_head() {
    echo '<style>.mainheadlinewrapperpage {
                        background-image: url("/wp-content/themes/rocionahle/img/headers/'. rand(1, 8) . '.jpg");
                }</style>';
}
add_action( 'wp_head', 'nahle_head' );

function nahle_template_redirect() {
	remove_filter('the_content', 'sharing_display', 19);
	if(is_single()) {
		add_filter('the_content', 'sharing_display', 8);
	}
}
add_action('template_redirect', 'nahle_template_redirect');


//remove_filter('the_excerpt', 'sharing_display', 19 );

