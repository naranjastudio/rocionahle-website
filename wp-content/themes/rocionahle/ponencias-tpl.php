<?php

/*Template Name: Ponencias
*/
get_header(); ?>


	<!-- menu -->
	<nav class="navbar sticker wrapper100percent">

		<div class="navbar-header">
			<button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse" type="button">
				<span class="sr-only"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<div class="navbar-brand">
				<?php if ( get_theme_mod( 'logo' ) ) : ?>
					<div class='site-logo'>
						<a href='<?php echo esc_url( home_url( '/' ) ); ?>'
						   title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'><img
								src='<?php echo esc_url( get_theme_mod( 'logo' ) ); ?>'
								alt='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>'></a>
					</div>
				<?php else : ?>
					<hgroup>
						<h2 class='site-title'><a href='<?php echo esc_url( home_url( '/' ) ); ?>'
						                          title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>'
						                          rel='home'><?php bloginfo( 'name' ); ?></a></h2>

						<h3 class='site-description'><?php bloginfo( 'description' ); ?></h3>
					</hgroup>
				<?php endif; ?>
			</div>
		</div>
		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<div class="cl-effect-12">
				<?php wp_nav_menu( array(
						'theme_location' => 'header-menu',
						'menu'           => 'top_menu',
						'menu_class'     => 'nav navbar-nav pull-right'
					)
				);
				?>
			</div>
		</div>
	</nav>
	<!-- menu end -->

	<div id="wrapperpages">

		<?php
		while ( have_posts() ) : the_post(); ?>

			<div class="mainheadlinewrapperpage wrapper100percent">

				<div class="mainheadlinepage container">
					<h1 class="entry-title"><?php the_title(); ?></h1>

				</div>
			</div>

			<div class="wrapper100percent contentwrapper">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-md-6" style="margin-bottom: 40px">
							He sido ponente en diversos foros y ponencias sobre Petroquímica y Energía en Universidades, cámaras
							comerciales, espacios de debate y asociaciones independientes. Ponente en el Foro de la Reforma Energética
							en el 2008, en el Senado de la República y en el Foro de la Reforma Energética en el 2013 también en el
							Senado de la Republica, sobre los temas de Petrolíferos y Petroquímicos.
						</div>
					</div>
				</div>

				<div class="container ponencias-cont">
					<div class="row">
						<div class="col-sm-6 col-md-4 vyear-cont vyear-2008">
							<div class="video-container">
								<a href="https://www.youtube.com/watch?v=cqkcKxhA72I" target="_blank"
								   rel="wp-video-lightbox[ponencias]">
									<div class="video-img-cont"><img
											src="/wp-content/uploads/2014/09/ponencia_en_el_senado-e1428344107312.jpg"></div>
									<h3>Ponencia en Reforma Energética en el Senado de la República 2008</h3>
								</a>
							</div>
						</div>
						<div class="col-sm-6 col-md-4 vyear-cont vyear-2013">
							<div class="video-container">
								<a href="https://www.youtube.com/watch?v=_HDty5au5Yc" target="_blank"
								   rel="wp-video-lightbox[ponencias]">
									<div class="video-img-cont"><img
											src="/wp-content/uploads/2014/09/reforma_entreguista.jpg"></div>
									<h3>El proyecto de la Reforma Entreguista</h3>
								</a>
							</div>
						</div>
						<div class="col-sm-6 col-md-4 vyear-cont vyear-2013">
							<div class="video-container">
								<a href="https://www.youtube.com/watch?v=phwRKN7DDWE" target="_blank"
								   rel="wp-video-lightbox[ponencias]">
									<div class="video-img-cont"><img src="/wp-content/uploads/2014/09/Ing-Rocío-Nahle-Participación.jpg">
									</div>
									<h3>Ponencia en el Senado de la República Reforma Energética 2013</h3>
								</a>
							</div>
						</div>


						<div class="col-sm-6 col-md-4 vyear-cont vyear-2013">
							<div class="video-container">
								<a target="_blank"
								   href="http://nuevaprensa.net/en-juicio-reuniendo-visiones-sobre-la-reforma-energetica/">
									<div class="video-img-cont"><img
											src="/wp-content/uploads/2014/09/Ponencia-en-el-ITESM-sobre-la-Reforma-Energética-2013.jpg"></div>
									<h3>Ponencia en el ITESM sobre la Reforma Energética 2013</h3>
								</a>
							</div>
						</div>
						<div class="col-sm-6 col-md-4 vyear-cont vyear-2014">
							<div class="video-container">
								<a href="https://www.youtube.com/watch?v=l5wKx0QvT18" target="_blank"
								   rel="wp-video-lightbox[ponencias]">
									<div class="video-img-cont"><img
											src="/wp-content//uploads/2014/09/mesa_redonda_UNAM.jpg"></div>
									<h3>Resultados e Implicaciones de la Reforma Energética en la UNAM</h3>
								</a>
							</div>
						</div>
						<div class="col-sm-6 col-md-4 vyear-cont vyear-2014">
							<div class="video-container">
								<a href="https://www.youtube.com/watch?v=C1buhqR2Afc" target="_blank"
								   rel="wp-video-lightbox[ponencias]">
									<div class="video-img-cont"><img
											src="/wp-content/uploads/2014/09/leyes_secundarias-e1428342942343.jpg"></div>
									<h3>Análisis de Leyes Secundarias en la Cámara de Diputados</h3>
								</a>
							</div>
						</div>


						<div class="col-sm-6 col-md-4 vyear-cont vyear-2014">
							<div class="video-container">
								<a href="https://www.youtube.com/watch?v=gLv67E1ZjmM" target="_blank"
								   rel="wp-video-lightbox[ponencias]">
									<div class="video-img-cont"><img src="/wp-content/uploads/2014/09/SME-foro-Reforma-energetica.jpg">
									</div>
									<h3>SME Foro Leyes Secundarias</h3>
								</a>
							</div>
						</div>
						<div class="col-sm-6 col-md-4 vyear-cont vyear-2014">
							<div class="video-container">
								<a href="https://www.youtube.com/watch?v=a_N-Eucx8Og" target="_blank"
								   rel="wp-video-lightbox[ponencias]">
									<div class="video-img-cont"><img src="/wp-content/uploads/2014/09/SME-100-años-entrevista.jpg"></div>
									<h3>SME 100 Años Entrevista Rocío Nahle</h3>
								</a>
							</div>
						</div>
						<div class="col-sm-6 col-md-4 vyear-cont vyear-2014">
							<div class="video-container">
								<a href="https://www.youtube.com/watch?v=MHnoNi0VaUE" target="_blank"
								   rel="wp-video-lightbox[ponencias]">
									<div class="video-img-cont"><img
											src="/wp-content/uploads/2014/09/18_de_marzo.jpg"></div>
									<h3>18 de Marzo 2014</h3>
								</a>
							</div>
						</div>


						<div class="col-sm-6 col-md-4 vyear-cont vyear-2015">
							<div class="video-container">
								<a href="https://www.youtube.com/watch?v=3if_ISh0Jcg" target="_blank"
								   rel="wp-video-lightbox[ponencias]">
									<div class="video-img-cont"><img src="/wp-content/uploads/2014/09/La-Reforma-Energetica.jpg"></div>
									<h3>Entrevista Reforma Energética Agua Dulce</h3>
								</a>
							</div>
						</div>
						<div class="col-sm-6 col-md-4 vyear-cont vyear-2015">
							<div class="video-container">
								<a href="https://www.youtube.com/watch?v=XdU93XcXh4E" target="_blank"
								   rel="wp-video-lightbox[ponencias]">
									<div class="video-img-cont"><img src="/wp-content/uploads/2014/09/AMLO-y-Rocio-Nahle-en-Coatza.jpg">
									</div>
									<h3>Ponencia de AMLO en Coatzacoalcos Situación Económica y Política de México.</h3>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>

		<?php endwhile; ?>

	</div>



<?php get_footer(); ?>