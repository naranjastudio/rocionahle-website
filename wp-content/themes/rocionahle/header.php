<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset=<?php bloginfo( 'charset' ); ?> />
<meta name=viewport content="width=device-width, initial-scale=1">
<title><?php wp_title('|', true, 'right'); ?></title>
<link rel="icon" href="/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<!--[if lt IE 9]>
<script src=<?php echo get_template_directory_uri(); ?>/js/html5.js type=text/javascript></script>
<![endif]-->

<?php wp_head(); ?>
</head>
<body <?php body_class(); ?> data-spy="scroll">

<div class="headermore">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <a class="accordion-toggle-top fa fa-chevron-down collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                </a>
            </div>
        </div>
    </div>
</div>

<div id="collapseOne" class="headerwrapper panel-collapse collapse">

<div class="row headersearchform">    
<div class="col-lg-2 col-md-3 col-sm-4">   
<?php if ( is_active_sidebar( 'sidebar-6' ) ) : ?>
<?php dynamic_sidebar( 'sidebar-6' ); ?>	
<?php endif; ?>
	<?php if(is_front_page()): ?>
    <audio autoplay="autoplay" controls="controls">
        <source src="/wp-content/themes/rocionahle/audio/morena_shout.mp3" />
    </audio>
	<?php endif; ?>
</div>
<div class="col-lg-10 col-md-9 col-sm-8">   
<?php if ( is_active_sidebar( 'sidebar-7' ) ) : ?>
<?php dynamic_sidebar( 'sidebar-7' ); ?>	
<?php endif; ?>			
</div>
</div>
</div>









