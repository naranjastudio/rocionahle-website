<?php

/*Template Name: Propuestas Index
*/
get_header(); ?>

	<!-- menu -->
	<nav class="navbar sticker wrapper100percent">

		<div class="navbar-header">
			<button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse" type="button">
				<span class="sr-only"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<div class="navbar-brand">
				<?php if ( get_theme_mod( 'logo' ) ) : ?>
					<div class='site-logo'>
						<a href='<?php echo esc_url( home_url( '/' ) ); ?>'
						   title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'><img
								src='<?php echo esc_url( get_theme_mod( 'logo' ) ); ?>'
								alt='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>'></a>
					</div>
				<?php else : ?>
					<hgroup>
						<h2 class='site-title'><a href='<?php echo esc_url( home_url( '/' ) ); ?>'
						                          title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>'
						                          rel='home'><?php bloginfo( 'name' ); ?></a></h2>

						<h3 class='site-description'><?php bloginfo( 'description' ); ?></h3>
					</hgroup>
				<?php endif; ?>
			</div>
		</div>
		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<div class="cl-effect-12">
				<?php wp_nav_menu( array(
						'theme_location' => 'header-menu',
						'menu'           => 'top_menu',
						'menu_class'     => 'nav navbar-nav pull-right'
					)
				);
				?>
			</div>
		</div>
	</nav>
	<!-- menu end -->

	<div id="wrapperpages">

<?php
while ( have_posts() ) : the_post(); ?>

	<div class="mainheadlinewrapperpage wrapper100percent">

		<div class="mainheadlinepage container">
			<h1 class="entry-title"><?php the_title(); ?></h1>

		</div>
	</div>

	<div class="propuestas-index">
		<div class="wrapper100percent section1">
			<div class="sectionwrapper"></div>
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<a id="propuesta1"></a>
						<p class="lead" style="margin-bottom: 60px">Estas son las Propuestas Legislativas que promoveré en caso de ser elegida como Diputada Federal por el Distrito XI del estado de Veracruz en las elecciones de 2015.</p>
					</div>
					<div class="col-sm-6">
						<h2 class="propuesta-headline">1. Nuevo puente sobre el Río Coatzacoalcos.</h2>
						<p>Promover la asignación de recursos necesarios para la construcción de otro puente sobre el Río
						Coatzacoalcos, que permita el fácil y libre acceso a la zona industrial y municipios del sur de
						Veracruz.</p>
					</div>
					<div class="col-sm-6">
						<div class="embed-container">
							<p><iframe width="560" height="315" src="https://www.youtube.com/embed/5O5XDU9zGck" frameborder="0" allowfullscreen></iframe></p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="wrapper100percent section1color section1">
			<a id="propuesta2"></a>
			<div class="sectionwrapper"></div>
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-sm-push-6">
						<h2 class="propuesta-headline">2. Promover el empleo en la industria Petroquímica.</h2>
						<p>Impulsar el desarrollo de la industria petroquímica y petrolífera promoviendo empleo y protegiendo
						el derecho a recibir salarios dignos y bien remunerados.</p>
					</div>
					<div class="col-sm-6 col-sm-pull-6">
						<div class="embed-container">
							<p><img src="/wp-content/uploads/2014/09/petroleo_empleos.jpg"></p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="wrapper100percent section1">
			<a id="propuesta3"></a>
			<div class="sectionwrapper"></div>
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<h2 class="propuesta-headline">3. Elevar la calidad de la educación.</h2>
						<p>Asignar recursos para elevar la calidad de la educación y becas para programas de especialización a
						estudiantes y maestros de Coatzacoalcos, Agua Dulce y Nanchital.</p>
					</div>
					<div class="col-sm-6">
						<div class="embed-container">
							<p><img src="/wp-content/uploads/2014/09/mejor_escuela_3.jpg"></p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="wrapper100percent section1color section1">
			<a id="propuesta4"></a>
			<div class="sectionwrapper"></div>
			<div class="container">
				<div class="row">
					<div class="col-sm-6  col-sm-push-6">
						<h2 class="propuesta-headline">4. Incrementar el presupuesto a Universidades.</h2>
						<p>Incrementar el presupuesto asignado a las Universidades del sur de Veracruz para el desarrollo de
						investigación, ciencia y tecnología.</p>
					</div>
					<div class="col-sm-6 col-sm-pull-6">
						<div class="embed-container">
							<p><img src="/wp-content/uploads/2014/09/mas_recurso_universidades.jpg"></p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="wrapper100percent section1">
			<a id="propuesta5"></a>
			<div class="sectionwrapper"></div>
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<h2 class="propuesta-headline">5. Implementación de guarderías gratuitas.</h2>
						<p>Promover la asignación de recursos para implementar guarderías y estancias infantiles gratuitas y de
						doble turno para albergar y atender a los hijos de las familias trabajadoras en Coatzacoalcos, Agua Dulce
						y Nanchital.</p>
					</div>
					<div class="col-sm-6">
						<div class="embed-container">
							<p><img src="/wp-content/uploads/2014/09/guarderias_gratuitas.jpg"></p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="wrapper100percent section1color section1">
			<a id="propuesta6"></a>
			<div class="sectionwrapper"></div>
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-sm-push-6">
						<h2 class="propuesta-headline">6. Transparencia absoluta y rendición de cuentas.</h2>
						<p>Legislar para lograr una absoluta transparencia y rendición de cuentas de todos los funcionarios
						públicos y autoridades representativas, con esto lograr combatir la corrupción e iniciar con la cultura de
						gobiernos honrados.</p>
					</div>
					<div class="col-sm-6 col-sm-pull-6">
						<div class="embed-container">
							<p><p><img src="/wp-content/uploads/2014/09/transparencia.jpg"></p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="wrapper100percent section1">
			<div class="sectionwrapper"></div>
			<div class="col-xs-12">
				<h4 style="text-align: center; clear: both;">Contáctanos</h4>
				<?= do_shortcode('[contact-form-7 id="4" title="Contact form 1"]'); ?>
			</div>
 		</div>
	</div>

<?php endwhile; ?>

<?php get_footer(); ?>